$(document).ready(function () {
	poster();
	posterSlajd();
	proizvod();
});


var posteri = [
	["Laptop računari", "Velika ponuda po niskim cenama", "laptopSir"],
	["Macintosh računari", "Ovlašćeni distributer", "macSir"],
	["Samsung Galaxy", "Brend kome se veruje", "galSir"],
	["Iphone", "Uživajte u jednostavnosti", "ajSir"]
];

function poster() {
	var sekPosteri = document.getElementById("posteri");
	var ispis = "";
	for (let i = 0; i < posteri.length; i++) {
		ispis = "";
		let artikal = document.createElement("article");
		artikal.classList.add("poster");
		artikal.style.backgroundImage = "url('images/" + posteri[i][2] + ".jpg')";
		ispis += "<div><h3>" + posteri[i][0] + "</h3><p>" + posteri[i][1] + "</p></div>";
		artikal.innerHTML = ispis;
		sekPosteri.appendChild(artikal);
	}
	sekPosteri.firstElementChild.classList.add("vidi");
}


function posterSlajd() {
	var trenutni = $("#posteri article.vidi");
	var sledeci = trenutni.next().length ? trenutni.next() : trenutni.parent().children(":first");
	trenutni.removeClass("vidi");
	sledeci.addClass("vidi");
	setTimeout(posterSlajd, 3000);
}


var proizvodi = [
	["gal9", "Galaxy S9", "Novo iz S serije",
		"TouchScreen, 5.8, 1440 x 2960 18.5:9 odnos, 570 ppi, OS Android 8.0  Interna memorija 64/128/256 GB, 4 GB RAM", 549],
	["lgk10", "LG K10", "Za svačiji džep",
		"TouchScreen, 5.3, 1280 x 720, 277 ppi, Qualcomm Snapdragon 410, OS Android 5.1.1 Lollipop", 69],
	["iphone6s",  "Iphone 6s", "Odličan izbor",
		"TouchScreen, 4.7, 750 x 1334, 326 ppi, Procesor Dual-Core 1.84 GHz Twister, OS iOS 9, nadogradiv na iOS 9.1", 419],
	["hpG6","HP 250 G6", "Pouzdan i brz",
		"15.6 FullHD LED (1920x1080), Intel Core i5-7200U 2.5GHz, 8GB, 1TB HDD, Intel HD Graphics, DVDRW, Win 10 Pro, ", 649],
	["imac",  "iMac", "Živahne boje",
		"21.5 inča LED 1920‑by‑1080, 2.3GHz Intel Core i5, 8GB RAM 2133MHz DDR4 ", 999],
	["lenovo",  "Lenovo V330", "Za svakodnevne potrebe",
		"15.6 FullHD LED (1920x1080), Intel procesor, RAM 4GB, 128GB SSD, Nema OS.", 399]
];

function proizvod() {
	var sekProizvodi = document.getElementById("proizvodi");
	for (let i = 0; i < proizvodi.length; i++) {
		let ispis = "";
		let artikal = document.createElement("article");
		artikal.id = "pr" + i;
		ispis += "<img src='images/" + proizvodi[i][0] + ".jpg' alt='" + proizvodi[i][1] + " slika'/><h3>" +
			proizvodi[i][1] + "</h3><p>" + proizvodi[i][2] + "</p><p>" + proizvodi[i][4] +
			"&euro; </p><button>Detalji</button><button>Kupi</button>";
		artikal.innerHTML = ispis;
		sekProizvodi.appendChild(artikal);
	}
	$("#proizvodi article img").on("click", detaljnije);
	$("#proizvodi article button:first-of-type").on("click", detaljnije);
	$("#proizvodi article button:last-of-type").on("click", function(){window.location = "prijava.html";});
}

function detaljnije() {
	var bledi = document.createElement("div");
	bledi.classList.add("bledi");
	document.body.appendChild(bledi);
	
	var det = document.createElement("div");
	det.innerHTML = puniDetalje(this);
	det.setAttribute("id", "preklop");
	document.body.appendChild(det);
	
	document.body.style.overflow = "hidden";
	
	bledi.addEventListener("click", zatvaraj);
	document.getElementById("zatvori").addEventListener("click", zatvaraj);
	$("#preklop button").on("click", function(){window.location = "prijava.html";});

	function zatvaraj() {
		bledi.parentNode.removeChild(bledi);
		det.parentNode.removeChild(det);
		document.body.style.overflow = "auto";
	}

}

function puniDetalje(th) {
	var ide = th.parentElement.id;
	var prId = ide.substr(2, 1);
	var ispis = "";

	ispis += "<img src='images/" + proizvodi[prId][0] + ".jpg' alt='" + proizvodi[prId][1] +
		" slika'/><div><h3>" + proizvodi[prId][1] + "</h3><p>" + proizvodi[prId][2] + "</p><p>" + proizvodi[prId][3] +
		"</p><span>" + proizvodi[prId][4] + "&euro; </span><button>Kupi</button></div><i id='zatvori' class='fa fa-times-circle'></i>";

	return ispis;
}