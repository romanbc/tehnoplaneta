$(document).ready(function () {
	navigacija();
	navigacijaF();
	$(window).on("scroll", skrol);
	$("#vrati").on("click", vrati);
});


var meni = [["Početna", "index.html"],["Nalog", "prijava.html"], ["Autor", "autor.html"],["Dokumentacija", "dokumentacija.pdf"]];

function navigacija() {
	var ispis = "";
	var loc = window.location.pathname;
	for (let i = 0; i < meni.length; i++) {
		let regHref = new RegExp(meni[i][1] + "$");
		let aktiv = regHref.test(loc) ? " class='aktivan'>" : ">";
		ispis += "<li" + aktiv + "<a href='" + meni[i][1] + "'>" + meni[i][0] + "</a></li>";
	}
	document.getElementById("nav").innerHTML = ispis;
}


function navigacijaF() {
	var ispis = "";
	for (let i = 0; i < meni.length; i++) {
		ispis += "<li><a href='" + meni[i][1] + "'>" + meni[i][0] + "</a></li>";
	}
	document.getElementsByTagName("footer")[0].classList.add("foot");
	document.getElementById("navF").innerHTML = ispis;
}

function skrol() {
	if ($("html").scrollTop() > 180) {
		$("#vrati").css("display", "block");
	} else
		$("#vrati").css("display", "none");
}

function vrati() {
	$("html").animate({ scrollTop: 0 }, 350);
}
