$(document).ready(function () {
	dodajListenere();
	lozinkaPonovo.addEventListener("change", promenaLozinkePonovo);
	lozinka.addEventListener("change", promenaLozinke);
	document.fSignIn.submit.addEventListener("click", valIn);
	document.fSignUp.submit.addEventListener("click", valIn);
});

var regMejl = /^\w+([.-]?[\w\d]+)*@\w+([.-]?[\w]+)*(\.\w{2,4})+$/;
var regLozinka = /^(?=.*\d)(?=.*[a-zčćžšđ])(?=.*[A-ZČĆŽŠĐ])[0-9a-zčćžšđA-ZČĆŽŠĐ]{6,}$/;
var regIme = /^[A-Z][a-z]{1,15}$/;
var regPrezime = /^[A-Z][a-z]{1,15}$/;
var regLozinkaPonovo = /^(?=.*\d)(?=.*[a-zčćžšđ])(?=.*[A-ZČĆŽŠĐ])[0-9a-zčćžšđA-ZČĆŽŠĐ]{6,}$/;

var lozinka = document.fSignUp.tbLozinka;
var lozinkaPonovo = document.fSignUp.tbLozinkaPonovo;
var porukaPonovo = lozinkaPonovo.nextElementSibling;

function dodajListenere() {
	for (let i = 0; i < document.forms.length; i++) {
		for (let j = 0; j < document.forms[i].length - 1; j++) {
			document.forms[i][j].addEventListener("change", function () { promenaUnosa(this); });
		}
	}
}

function promenaUnosa(ovaj) {
	var name = ovaj.getAttribute("name");
	var reg = "reg" + name.substr(2);
	var poruka = ovaj.nextElementSibling;
	poruka.innerHTML = "";

	if (ovaj.value.trim() == "") {
		ovaj.classList.remove("pogresno", "dobro");
	} else if (window[reg].test(ovaj.value.trim())) {
		ovaj.classList.remove("pogresno");
		ovaj.classList.add("dobro");
	} else {
		ovaj.classList.remove("dobro");
		ovaj.classList.add("pogresno");
	}
}

function valIn(event) {
	event.preventDefault();
	var forma = this.parentElement;
	var imeForma = forma.name;

	var validna = true;

	for (let i = 0; i < document.forms[imeForma].length - 1; i++) {
		ispisiPoruku(document.forms[imeForma][i]);
	}

    var daLiJeRegistracija = this.getAttribute("id") == "submitR";

	if (daLiJeRegistracija && !iste()) {
		validna = false;
		porukaPonovo.innerHTML = "Lozinke se ne poklapaju";
	} 

	if (validna) {
		alert("Uspešan zahtev");
	}

	function ispisiPoruku(polje) {
		var poruka = polje.nextElementSibling;
		var name = polje.getAttribute("name");
		var reg = "reg" + name.substr(2);
		var ime = name.substr(2);

		if (polje.value.trim() == "") {
			poruka.innerHTML = "" + ime + " je obavezno polje";
			validna = false;
		} else if (!window[reg].test(polje.value.trim())) {
			poruka.innerHTML = ime + " nije u ispravnom formatu.";
			validna = false;
		} else {
			poruka.innerHTML = "";
		}
	}
}

function iste() {
	var jesu = false;
	if (lozinka.value.trim() == lozinkaPonovo.value.trim()) {
		jesu = true;
	}
	return jesu;
}

function promenaLozinke() {
	porukaPonovo.innerHTML = "";
	if (lozinkaPonovo.value.trim() == "") {
		lozinkaPonovo.classList.remove("pogresno", "dobro");
	} else if (iste() && regLozinkaPonovo.test(lozinkaPonovo.value.trim())) {
		lozinkaPonovo.classList.remove("pogresno");
		lozinkaPonovo.classList.add("dobro");
	} else {
		lozinkaPonovo.classList.remove("dobro");
		lozinkaPonovo.classList.add("pogresno");
	}
}

function promenaLozinkePonovo() {
	porukaPonovo.innerHTML = "";
	if (!iste()) {
		lozinkaPonovo.classList.remove("dobro");
		lozinkaPonovo.classList.add("pogresno");
	}
	if (lozinkaPonovo.value.trim() == "") {
		lozinkaPonovo.classList.remove("dobro", "pogresno");
	}
}










